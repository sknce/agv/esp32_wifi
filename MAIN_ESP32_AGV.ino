/********************************************************************************************************************
 This is the project responsible for WiFi communication of an application written in Qt with STM32
 Written by Rafał Szymura for SKN Control Enginners.
 ********************************************************************************************************************/
#include "includes.h"
//---------------------------------------------------------------------------------------------------------------------------

// Define UART pins
#define UART_TX_PIN 17
#define UART_RX_PIN 16
HardwareSerial SerialPort(2); // Use hardware UART (UART2) on ESP32
//---------------------------------------------------------------------------------------------------------------------------
// WiFi parameters
#define LOCAL_SSID "TP-LINK_498530"
#define LOCAL_PASS "haslo_sieci"
//---------------------------------------------------------------------------------------------------------------------------
const int port = 12; // TCP server port
WiFiServer server(port); // Sets up a TCP server on the port (WiFi.h)
WiFiClient client; // Create an instance of the WiFiClient class, which is used for establishing and managing client connections over a Wi-Fi
//---------------------------------------------------------------------------------------------------------------------------
IPAddress Actual_IP; // Variable for the IP
//---------------------------------------------------------------------------------------------------------------------------
// Definitions of your desired intranet created by the ESP32
IPAddress PageIP(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress ip;
//---------------------------------------------------------------------------------------------------------------------------
#define USE_INTRANET // Comment out to enable Access Point (AP) mode!
//---------------------------------------------------------------------------------------------------------------------------
// Acces Point settings
#define AP_SSID "AGV_ESP32_Control"
#define AP_PASS "AGV123"
//---------------------------------------------------------------------------------------------------------------------------
#define LED_BUILTIN 2

#define BUZZER_PIN 18
const int melody[] = { // Buzzer song
  NOTE_C4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_B4, NOTE_C5
};
const int noteDuration = 200;
//-------------------------------Standard communication settings-------------------------------------------------------------
void setup() {
  Serial.begin(115200);             // Initialize serial communication at 115200 baud rate
  pinMode(LED_BUILTIN, OUTPUT);     // Set the built-in LED pin as an output
  digitalWrite(LED_BUILTIN, HIGH);  // Turn on the built-in LED
  pinMode(BUZZER_PIN, OUTPUT);

  WiFiConnect(); // Custom function for network connection (in .ino file)
  
  // Initialize UART 2 communication
  SerialPort.begin(115200, SERIAL_8N1, UART_RX_PIN, UART_TX_PIN);

  // Set the START values
  dataPacket.mode = MODE_OFF;
  dataPacket.control = ACCESSORY_OFF;
  dataPacket.steering = STOP;
  dataPacket.additional = 0;

// Initialize OLED display
// if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
//     Serial.println(F("SSD1306 allocation failed"));
//     for(;;); // Don't proceed, loop forever
//   }
//   delay(2000); // Pause for 2 seconds
//   display.clearDisplay();  // Clear the buffer
//   display.drawBitmap(0, 0, image_data_Image, 128, 64, 1); // Draw bitmap on the screen
//   display.display();
}
//---------------------------------------------------------------------------------------------------------------------------
void loop() {
  client = server.available();  // Check for incoming client connections
  /*
  for (int i = 0; i < 8; ++i) {
    tone(BUZZER_PIN, melody[i], noteDuration);
    delay(noteDuration);
  }
  noTone(BUZZER_PIN);
*/
  if (client) {
    Serial.println("New incoming connection");  // Print a message for a new incoming connection
    Serial.println("--------------------");

    while (client.connected()) {
      if (client.available()) {
        // BYTES table (uint8_t = byte)
        uint8_t receivedData[19]; // Create a buffer for received data: frame 4 int elements

        if (client.available() >= sizeof(receivedData)) {
          client.read(receivedData, sizeof(receivedData)); // Read bytes data (uint8_t)

          // int ENUMnumbers[FRAME_ELEMENTS];
          // for (int i = 0; i < FRAME_ELEMENTS; i++) {
          //   ENUMnumbers[i] = *((int*)(receivedData + i * sizeof(int))); // Convert bytes to integers
          // }
          
          // dataPacket.mode = static_cast<OperatingMode>(ENUMnumbers[0]); // Convert an int value to the OperatingMode enum type and assigning it to the mode member of the dataPacket structure
          // dataPacket.control = static_cast<ControlValues>(ENUMnumbers[1]);
          // dataPacket.steering = static_cast<SteeringValues>(ENUMnumbers[2]);
          // dataPacket.additional = ENUMnumbers[3];

          // displayInfoOnOLED(dataPacket);
          for(int i=0;i<19;i++){
            char temp_buf;
            // sprintf(temp_buf,"%02x",receivedData[i]);
            Serial.println(receivedData[i]);
          }
          char txtbuffer[50];
          sprintf(txtbuffer, "---0x%2X len: %d", receivedData,sizeof(receivedData));
          Serial.println(txtbuffer);
          
          // Send data via UART2 to STM32
          SerialPort.write(receivedData, sizeof(receivedData)); // Send the byte array to the STM32, no need for Serialization

          // Return data to Qt app
          client.write(receivedData, sizeof(receivedData)); // Send the byte array to the client
        }
      }
    }
    client.stop();  // Close the client connection
    Serial.println("Connection closed");  // Print a message indicating connection closure
  }
}
