// Custom function for network connection
void WiFiConnect(){
  Serial.println("\nWiFi connecting...");

  //#define USE_INTRANET - connect to your home network
#ifdef USE_INTRANET
  WiFi.begin(LOCAL_SSID); // Connect to WiFi using the provided SSID and password
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print("."); // Display dots while waiting for WiFi connection
  }
  Actual_IP = WiFi.localIP();
  printWifiStatus();
#endif

  // #define USE_INTRANET is disabled - creat Access Point
#ifndef USE_INTRANET
  WiFi.softAP(AP_SSID, AP_PASS);
  delay(100);
  WiFi.softAPConfig(PageIP, gateway, subnet);
  delay(100);
  Actual_IP = WiFi.softAPIP();
  Serial.print("\nSSID: ");
  Serial.println(AP_SSID);
  Serial.print("AP IP address: "); 
  Serial.println(Actual_IP);
#endif

  Serial.print("Port ");
  Serial.println(port);  // Display the port number

  digitalWrite(LED_BUILTIN, LOW);  // Turn off the built-in LED
  Serial.println("AGV ESP32 v1.0");  // Print identification message
  Serial.println("WiFi connected!");  // Print a message indicating WiFi connection success
  
  server.begin();  // Start the server
} 
  
