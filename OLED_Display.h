#include <Wire.h> // To use I2C
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET -1 // If no reset pin -> -1
#define SCREEN_ADDRESS 0x3C 

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

static const uint8_t PROGMEM image_data_Image[1024] = {
  // https://randomnerdtutorials.com/esp32-ssd1306-oled-display-arduino-ide/
    // ████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
    // ████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
    // ████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
    // █████████████████████████████████████████████████████████████∙∙∙∙∙∙█████████████████████████████████████████████████████████████
    // ████████████████████████████████████████████████████████████∙█████∙█████████████████████████████████████████████████████████████
    // █████████████████████████████████████████████████████∙∙█████∙█████∙██████∙██████████████████████████████████████████████████████
    // ████████████████████████████████████████████████████∙█∙∙█∙∙∙∙██████∙∙∙██∙█∙∙████████████████████████████████████████████████████
    // ██████████████████████████████████████████████████∙∙███∙∙█████████████∙∙███∙∙███████████████████████████████████████████████████
    // █████████████████████████████████████████████████∙███████████████████████████∙██████████████████████████████████████████████████
    // █████████████████████████████████████████████████∙███████████████████████████∙██████████████████████████████████████████████████
    // ██████████████████████████████████████████████████∙██████████████████████████∙██████████████████████████████████████████████████
    // █████████████████████████████████████████████████∙██████████████████████████∙███████████████████████████████████████████████████
    // ████████████████████████████████████████████████∙████████████∙∙∙∙∙∙████████∙████████████████████████████████████████████████████
    // ███████████████████████████████████████████████∙██████████∙∙∙██████∙∙∙████∙█████████████████████████████████████████████████████
    // ████████████████████████████████████████████∙∙∙∙████████∙∙████████████∙███∙█████████████████████████████████████████████████████
    // ███████████████████████████████████████████∙███████████∙███████████████∙∙∙██████████████████████████████████████████████████████
    // ███████████████████████████████████████████∙█████████∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙█████████∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙█████████∙█∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // █████████████████████████████████████████∙█████████∙██∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙███████∙███∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ███████████████████████████████████████████∙██████∙███∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ███████████████████████████████████████████∙█████∙████∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙██████∙████∙∙∙∙∙∙∙███████████████████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙█████∙█████∙∙∙∙∙∙∙███████████████████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙█████∙█████∙∙∙∙∙∙∙███████████████████████████████████████████████████████████████████
    // ████████████████████████████████████████∙∙∙█████∙█████∙∙∙∙∙∙∙███████████████████████████████████████████████████████████████████
    // ███████████████████████████████████████∙████████∙█████∙∙∙∙∙∙∙∙∙∙∙∙∙█████████████████████████████████████████████████████████████
    // ███████████████████████████████████████∙████████∙█████∙∙∙∙∙∙∙∙∙∙∙∙∙█████████████████████████████████████████████████████████████
    // ███████████████████████████████████████∙████████∙█████∙∙∙∙∙∙∙∙∙∙∙∙∙█████████████████████████████████████████████████████████████
    // ███████████████████████████████████████∙████████∙█████∙∙∙∙∙∙∙∙∙∙∙∙∙█████████████████████████████████████████████████████████████
    // ███████████████████████████████████████∙████████∙█████∙∙∙∙∙∙∙∙∙∙∙∙∙█████████████████████████████████████████████████████████████
    // ████████████████████████████████████████∙███████∙█████∙∙∙∙∙∙∙∙∙∙∙∙∙█████████████████████████████████████████████████████████████
    // ████████████████████████████████████████∙∙∙█████∙█████∙∙∙∙∙∙∙███████████████████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙█████∙█████∙∙∙∙∙∙∙███████████████████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙█████∙█████∙∙∙∙∙∙∙███████████████████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙██████∙████∙∙∙∙∙∙∙███████████████████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙∙█████∙████∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ███████████████████████████████████████████∙██████∙███∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙███████∙███∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // █████████████████████████████████████████∙█████████∙██∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // █████████████████████████████████████████∙∙█████████∙█∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ██████████████████████████████████████████∙██████████∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ███████████████████████████████████████████∙██████████∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙∙███████████████████████████████████████████████████████
    // ███████████████████████████████████████████∙███████████∙███████████████∙∙∙██████████████████████████████████████████████████████
    // ████████████████████████████████████████████∙∙∙█████████∙∙███████████∙∙███∙█████████████████████████████████████████████████████
    // ███████████████████████████████████████████████∙██████████∙∙∙█████∙∙∙█████∙███████████∙∙∙∙∙∙∙∙∙∙∙███████████∙∙∙█████∙∙∙█████████
    // ████████████████████████████████████████████████∙████████████∙∙∙∙∙█████████∙██████████∙∙∙∙∙∙∙∙∙∙∙███████████∙∙∙█████∙∙∙█████████
    // █████████████████████████████████████████████████∙██████████████████████████∙█████████∙∙∙∙∙∙∙∙∙∙∙███████████∙∙∙█████∙∙∙█████████
    // ████████████████████████████████████████████████∙███████████████████████████∙█████████∙∙∙█████∙∙∙███████████∙∙∙∙███∙∙∙∙█████████
    // ████████████████████████████████████████████████∙∙███████████████████████████∙████████∙∙∙█████∙∙∙███████████∙∙∙∙███∙∙∙∙█████████
    // █████████████████████████████████████████████████∙███████████████████████████∙████████∙∙∙█████∙∙∙████████████∙∙∙███∙∙∙██████████
    // ██████████████████████████████████████████████████∙∙███∙∙∙███████████∙∙∙███∙∙█████████∙∙∙█████∙∙∙████████████∙∙∙███∙∙∙██████████
    // ████████████████████████████████████████████████████∙∙∙∙██∙∙∙██████∙∙███∙█∙███████████∙∙∙∙∙∙∙∙∙∙∙██∙∙∙∙∙∙∙∙██∙∙∙███∙∙∙██████████
    // ██████████████████████████████████████████████████████∙█████∙█████∙██████∙████████████∙∙∙∙∙∙∙∙∙∙∙██∙∙∙∙∙∙∙∙∙██∙∙███∙∙███████████
    // ████████████████████████████████████████████████████████████∙█████∙███████████████████∙∙∙∙∙∙∙∙∙∙∙██∙∙∙∙∙∙∙∙∙██∙∙∙█∙∙∙███████████
    // █████████████████████████████████████████████████████████████∙∙∙∙∙∙███████████████████∙∙∙∙∙∙∙██████∙∙∙███∙∙∙██∙∙∙█∙∙∙███████████
    // ██████████████████████████████████████████████████████████████████████████████████████∙∙∙∙∙∙∙∙∙████∙∙∙███∙∙∙██∙∙∙█∙∙∙███████████
    // ██████████████████████████████████████████████████████████████████████████████████████∙∙∙█∙∙∙∙∙∙███∙∙∙∙∙∙∙∙∙███∙∙∙∙∙████████████
    // ██████████████████████████████████████████████████████████████████████████████████████∙∙∙███∙∙∙∙∙██∙∙∙∙∙∙∙∙∙███∙∙∙∙∙████████████
    // ██████████████████████████████████████████████████████████████████████████████████████∙∙∙█████∙∙∙██∙∙∙∙∙∙∙∙∙███∙∙∙∙∙████████████
    // █████████████████████████████████████████████████████████████████████████████████████████████████████████∙∙∙█████∙∙█████████████
    // ████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
    // ████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf7, 0xdf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf9, 0xf7, 0xdf, 0xbf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf4, 0x87, 0xe3, 0x4f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xce, 0x7f, 0xfc, 0xe7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xbf, 0xff, 0xff, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xbf, 0xff, 0xff, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0xff, 0xff, 0xfb, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xbf, 0xff, 0xff, 0xf7, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f, 0xf8, 0x1f, 0xef, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xc7, 0xe3, 0xdf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xf0, 0xff, 0x3f, 0xfd, 0xdf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xef, 0xfe, 0xff, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xef, 0xf8, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0xf0, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0xf4, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xbf, 0xec, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0xdc, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xef, 0xdc, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xef, 0xbc, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0xbc, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0x7c, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0x7c, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0x1f, 0x7c, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0x7c, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0x7c, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0x7c, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0x7c, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0x7c, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0x7f, 0x7c, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0x1f, 0x7c, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0x7c, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0x7c, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0xbc, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xcf, 0xbc, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xef, 0xdc, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0xdc, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xbf, 0xec, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0x9f, 0xf4, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xdf, 0xf8, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xef, 0xfc, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xef, 0xfe, 0xff, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xf1, 0xff, 0x3f, 0xf9, 0xdf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xff, 0xc7, 0xc7, 0xdf, 0xfc, 0x00, 0x7f, 0xf1, 0xf1, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f, 0xf8, 0x3f, 0xef, 0xfc, 0x00, 0x7f, 0xf1, 0xf1, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xbf, 0xff, 0xff, 0xf7, 0xfc, 0x00, 0x7f, 0xf1, 0xf1, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f, 0xff, 0xff, 0xf7, 0xfc, 0x7c, 0x7f, 0xf0, 0xe1, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x3f, 0xff, 0xff, 0xfb, 0xfc, 0x7c, 0x7f, 0xf0, 0xe1, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xbf, 0xff, 0xff, 0xfb, 0xfc, 0x7c, 0x7f, 0xf8, 0xe3, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xce, 0x3f, 0xf8, 0xe7, 0xfc, 0x7c, 0x7f, 0xf8, 0xe3, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf0, 0xc7, 0xe7, 0x5f, 0xfc, 0x00, 0x60, 0x18, 0xe3, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfd, 0xf7, 0xdf, 0xbf, 0xfc, 0x00, 0x60, 0x0c, 0xe7, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf7, 0xdf, 0xff, 0xfc, 0x00, 0x60, 0x0c, 0x47, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0x1f, 0xff, 0xfc, 0x07, 0xe3, 0x8c, 0x47, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x01, 0xe3, 0x8c, 0x47, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x40, 0xe0, 0x0e, 0x0f, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x70, 0x60, 0x0e, 0x0f, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x7c, 0x60, 0x0e, 0x0f, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x8f, 0x9f, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};

void displayInfoOnOLED(const DataPacket &dataPacket) {
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);

  char txtbuffer[100];
  sprintf(txtbuffer, "\n OperatingMode: %d\n ControlValues: %d\n SteeringValues: %d\n Additional: %d", dataPacket.mode, dataPacket.control, dataPacket.steering, dataPacket.additional);
  display.print(txtbuffer);
  display.display();
}