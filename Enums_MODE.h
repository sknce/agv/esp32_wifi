//---------------------------------------------------------------------------------------------------------------------------
#define FRAME_ELEMENTS 4 // Number of elements in the data frame

enum OperatingMode {
  MODE_OFF,
  MODE_AUTONOMOUS,
  MODE_REMOTE,
  MODE_CUSTOM,
};

enum ControlValues {
  ACCESSORY_OFF,
  LIGHT,
  ALARM,
  ACCESSORY,
};

enum SteeringValues {
  STOP,
  FORWARD,
  BACKWARD,
  LEFT,
  RIGHT,
};

OperatingMode mode; // Declares a variable
ControlValues control;
SteeringValues steering;

int additional;

//---------------------------------------------------------------------------------------------------------------------------
struct DataPacket {
  OperatingMode mode;
  ControlValues control;
  SteeringValues steering;
  int additional;
};

DataPacket dataPacket; // Instance of the structure